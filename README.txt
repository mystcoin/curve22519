Elliptic curve25519 cryptography ported to C from Java.

A copy of the original Curve25519.java source file from the Nxt repository is included
for reference. The original is at:

https://bitbucket.org/JeanLucPicard/nxt/src/5863e9acad3dcfa9292c3600c666536fbd97724f/src/java/nxt/crypto/Curve25519.java?at=master

The C file curve25519.c contains all of the original methods of the Curve25519 Java class 
plus a main function which serves as a convenient interface.

Usage: curve25519 -[gGsvk] hexString64 ...

curve25519 is the name of the executable (gcc curve25519.c -o curve25519).

-g: generate signing key pair, given random hexString64
    returns: signingPublicKey signingPrivateKey

-G: generate key agreement key pair, given random hexString64
    returns: keyAgreementPublicKey keyAgreementPrivateKey

-s: sign, given signatureHash keyAgreementPrivateKey signingPrivateKey
    returns: signatureValue

-v: verify, given signatureValue signatureHash signingPublicKey
    returns: keyAgreementPublicKey

-k: generate shared secret, given myKeyAgreementPrivateKey yourKeyAgreementPublicKey
    returns: sharedSecret

All input arguments hexString64 are 64-digit hex strings.
Output consists of one or two 64-digit hex strings, separated by a space.
Errors: reported to stderr; stdout is empty; exit code is 1.

Additionally, two bash test scripts are provided, both of which depend on the linux hash 
utility sha256sum:

sign.sh: tests signing and verification
shared.sh: tests shared secret generation (key agreement)
