#!/bin/bash

# Test script for curve25519 sign and verify
# Output: signature

secretPhrase="secret"
message="message"

CURVE="./curve25519" # path to curve25519 executable

function hexToBytes {
	echo $1 | sed 's/../\\x&/g'
}

function hash {
	local hash=($(echo -ne $1 | sha256sum))
	echo ${hash[0]}
}

Ps=($($CURVE -g $(hash "$secretPhrase")))
if (($?)); then exit 1; fi
P=${Ps[0]} # public key for signing
s=${Ps[1]} # private key for signing

m=$(hash $message) # message hash
Gk=($($CURVE -G $(hash $(hexToBytes $m$s))))
if (($?)); then exit 1; fi
G=${Gk[0]} # public key for key agreement
k=${Gk[1]} # private key for key agreement

mG=$(hash $(hexToBytes $m$G)) # signature hash
v=$($CURVE -s $mG $k $s) # signature value
if (($?)); then exit 1; fi
echo $v${mG^^} # full signature, upper case for esthetics

# verify, given m, v, mG and P

Y=$($CURVE -v $v $mG $P) # public key for key agreement, Y == G
if (($?)); then exit 1; fi

mY=$(hash $(hexToBytes $m$Y))
if [[ $mG != $mY ]]; then echo "Error: signature not verified"; exit 1; fi

exit 0
