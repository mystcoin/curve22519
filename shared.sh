#!/bin/bash

# Test script for curve25519 key agreement (shared secret)
# Output: Alice and Bob's shared secret

secretPhraseAlice="alice"
secretPhraseBob="bob"

CURVE="./curve25519" # path to curve25519 executable

function hexToBytes {
	echo $1 | sed 's/../\\x&/g'
}

function hash {
	local hash=($(echo -ne $1 | sha256sum))
	echo ${hash[0]}
}

secretPhraseAliceHash=$(hash "$secretPhraseAlice")
secretPhraseBobHash=$(hash "$secretPhraseBob")

GkAlice=($($CURVE -G $secretPhraseAliceHash))
if (($?)); then exit 1; fi
GAlice=${GkAlice[0]} # public key for key agreement
kAlice=${GkAlice[1]} # private key for key agreement

GkBob=($($CURVE -G $secretPhraseBobHash))
if (($?)); then exit 1; fi
GBob=${GkBob[0]} # public key for key agreement
kBob=${GkBob[1]} # private key for key agreement

SharedAlice=$($CURVE -k $kAlice $GBob)
if (($?)); then exit 1; fi

SharedBob=$($CURVE -k $kBob $GAlice)
if (($?)); then exit 1; fi

if [[ $SharedAlice != $SharedBob ]]; then echo "Error: shared secret does not match"; exit 1; fi
echo $SharedAlice

exit 0
